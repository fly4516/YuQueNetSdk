﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YuQueSdk.Dto
{
    /// <summary>
    /// 拥有能力
    /// </summary>
    public class AbilitiesDto
    {
        /// <summary>
        /// 是否有权限修改
        /// </summary>
        public bool Update { get; set; }

        /// <summary>
        /// 是否有权限销毁
        /// </summary>
        public bool Destroy { get; set; }
    }
}
