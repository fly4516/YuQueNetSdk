﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YuQueSdk.Dto
{
    /// <summary>
    /// 目录树形结构的输出对象
    /// </summary>
    public class TopicTree : Topic
    {
        /// <summary>
        /// 树结构的子集
        /// </summary>
        public List<TopicTree> Child { get; set; }
    }
}
