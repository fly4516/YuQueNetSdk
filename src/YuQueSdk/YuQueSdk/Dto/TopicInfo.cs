﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YuQueSdk.Dto
{
    /// <summary>
    /// 目录信息
    /// </summary>
    public class TopicInfo
    {
        /// <summary>
        /// 目录
        /// </summary>
        public List<Topic> Data { get; set; }
    }
}
