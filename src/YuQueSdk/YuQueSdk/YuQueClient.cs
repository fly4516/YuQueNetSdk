﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YuQueSdk.Dto;

namespace YuQueSdk
{
    /// <summary>
    /// 提供一些基础的方法
    /// </summary>
    public partial class YuQueClient
    {
        /// <summary>
        /// 语雀个人设置的skd
        /// </summary>
        private string _token;

        /// <summary>
        /// 语雀收集的用户标识
        /// </summary>
        private string _userAgent;

        /// <summary>
        /// 语雀基础的api域名
        /// </summary>
        private const string baseUrl = "https://www.yuque.com/api/v2/";

        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="token">语雀个人设置的skd</param>
        /// <param name="userAgent">语雀收集的用户标识</param>
        public YuQueClient(string token, string userAgent = "netCoreSdk")
        {
            _token = token;
            _userAgent = userAgent;
        }

        /// <summary>
        /// 获取单篇文档的详细信息
        /// </summary>
        /// <param name="docRepo">文档所属的知识库 如：https://www.yuque.com/yuque/developer/repo 中的【yuque/developer】</param>
        /// <param name="slug">文档的路径 如：https://www.yuque.com/yuque/developer/repo 中的【repo】</param>
        /// <returns>文档的详细信息</returns>
        public DocDetail GetDocDetail(string docRepo, string slug)
        {
            string jsonStr = HttpHelper.HttpGet($"{baseUrl}repos/{docRepo}/docs/{slug}", _token, _userAgent);
            return JsonConvert.DeserializeObject<DocDetail>(jsonStr);
        }

        /// <summary>
        /// 获取知识库下的目录
        /// </summary>
        /// <param name="docRepo">文档所属的知识库 如：https://www.yuque.com/yuque/developer/repo 中的【yuque/developer】</param>
        /// <returns>目录信息</returns>
        public List<Topic> GetRepoTopic(string docRepo)
        {
            string jsonStr = HttpHelper.HttpGet($"{baseUrl}repos/{docRepo}/toc", _token, _userAgent);
            return JsonConvert.DeserializeObject<TopicInfo>(jsonStr).Data;
        }

        /// <summary>
        /// 获取知识库下的目录（树结构）
        /// </summary>
        /// <param name="docRepo">文档所属的知识库 如：https://www.yuque.com/yuque/developer/repo 中的【yuque/developer】</param>
        /// <returns>目录信息</returns>
        public List<TopicTree> GetRepoTopicTree(string docRepo)
        {
            string jsonStr = HttpHelper.HttpGet($"{baseUrl}repos/{docRepo}/toc", _token, _userAgent);
            List<Topic> topicList = JsonConvert.DeserializeObject<TopicInfo>(jsonStr).Data;
            List<TopicTree> result = new List<TopicTree>();
            var firstList = topicList.Where(x => x.Level == 0);//第一级
            return ProcessTree(firstList, topicList);
        }

        /// <summary>
        /// 递归处理树结构
        /// </summary>
        /// <param name="child"></param>
        /// <param name="allList"></param>
        /// <returns></returns>
        private List<TopicTree> ProcessTree(IEnumerable<Topic> child, List<Topic> allList)
        {
            List<TopicTree> result = new List<TopicTree>();

            foreach (Topic item in child)
            {
                TopicTree topicTree = MapEntity(item);
                var childList = allList.Where(x => x.Parent_uuid == item.Uuid);
                if (childList.Count() > 0)
                {
                    topicTree.Child = ProcessTree(childList, allList);
                }
                result.Add(topicTree);
            }

            return result;
        }

        /// <summary>
        /// 未引用automap，直接转
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        private TopicTree MapEntity(Topic topic)
        {
            TopicTree result = new TopicTree
            {
                Child = new List<TopicTree>(),
                Child_uuid = topic.Child_uuid,
                Doc_id = topic.Doc_id,
                Id = topic.Id,
                Level = topic.Level,
                Open_window = topic.Open_window,
                Parent_uuid = topic.Parent_uuid,
                Prev_Uuid = topic.Prev_Uuid,
                Sibling_uuid = topic.Sibling_uuid,
                Title = topic.Title,
                Type = topic.Type,
                Url = topic.Url,
                Uuid = topic.Uuid,
                Visible = topic.Visible
            };

            return result;
        }
    }
}
