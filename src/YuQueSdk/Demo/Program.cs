﻿using System;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string token = "token值在语雀个人空间可以获取到";
            YuQueSdk.YuQueClient yuQueClient = new YuQueSdk.YuQueClient(token);

            #region 获取知识库目录

            var topics = yuQueClient.GetRepoTopic("yuque/developer");
            Console.WriteLine(topics.Count);

            #endregion

            #region 获取知识库目录树形结构

            var topicTrees = yuQueClient.GetRepoTopicTree("yuque/developer");
            Console.WriteLine(topicTrees.Count);

            #endregion

            #region 获取文档详情
            var detail = yuQueClient.GetDocDetail("yuque/developer", "doc");
            Console.WriteLine(detail.Data.Title);
            #endregion


        }
    }
}
